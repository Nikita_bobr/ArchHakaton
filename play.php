<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Socialhack | 3 измерения</title>
    <meta name="description" content="Socialhack | 3 измерения">
    <script src="https://aframe.io/releases/0.9.2/aframe.min.js"></script>
    <script src="//cdn.rawgit.com/donmccurdy/aframe-physics-system/v3.3.0/dist/aframe-physics-system.min.js"></script>
    <script src="https://cdn.rawgit.com/donmccurdy/aframe-extras/v4.1.2/dist/aframe-extras.min.js"></script>
    <script src="https://rawgit.com/fernandojsg/aframe-teleport-controls/master/dist/aframe-teleport-controls.min.js"></script>
    <script src="https://rawgit.com/rdub80/aframe-teleport-extras/master/dist/aframe-teleport-extras.min.js"></script>
  	<script src="https://unpkg.com/aframe-environment-component@1.1.0/dist/aframe-environment-component.min.js"></script>
  	<script src="https://cdn.statically.io/gh/darkwave/aframe-stl-model-component/7ed9bfa9/dist/aframe-stl-model-component.min.js"></script>
  	<script type="text/javascript" src="https://cdn.statically.io/gh/supermedium/superframe/897baf70/components/aabb-collider/dist/aframe-aabb-collider-component.min.js"></script>
  </head>
  <body>

    <a-scene id="main" background="color: #ffffff">
          <a-entity environment="preset: starry"></a-entity>
        <a-assets>
			<!-- <a-asset-item response-type="arraybuffer" id="castle" src="models/castle.stl"></a-asset-item> -->
			<!-- <img id="image-1" src="imgs/1.jpg"> -->
        </a-assets>


        <a-entity id="main_scene" visible="true"> 
        	<!-- Общий туннель -->
	        <!-- <a-entity stl-model="src: #tunnel" scale="0.07 0.07 0.07" rotation="0 0 0" position="18.64 1.12 -0.03" material="color: #7f7f7f; roughness: 1; metalness: 0"></a-entity> -->
        </a-entity>
        <a-entity id="imgs_scene" visible="false">
        	<a-image src="#image-1" depth="0.1" position="1.15 1 0"></a-image>
        </a-entity>


        <a-entity id="cameraRig">
	      <!-- camera -->
	      <a-entity id="head" camera wasd-controls look-controls></a-entity>
	      <!-- hand controls -->
	      <a-entity class="hand" static-body vive-controls="hand: left" id="left-hand" teleport-controls="cameraRig: #cameraRig; teleportOrigin: #head; "  ></a-entity>
	      <a-entity class="hand" static-body vive-controls="hand: right" id="right-hand" teleport-controls="cameraRig: #cameraRig; teleportOrigin: #head; " ></a-entity>
	    </a-entity>
        
    </a-scene>
    <script type="text/javascript">
    	var audio = document.createElement('audio');
    	function tts(text, callback) {

    		var url = "https://tts.voicetech.yandex.net/generate?"+
    			"key=069b6659-984b-4c5f-880e-aaedcfd84102"+
    			"&text="+encodeURI(text)+
    			"&format=mp3"+
    			"&lang=ru-RU"+
    			"&speaker=erkanyavas";
    		audio.src = url;
    		audio.load();
    		audio.onloadeddata = function () {
    			audio.play();
    		}
    		audio.onended = function () {
    			callback();
    		}
    	}
    </script>

  </body>
</html>
