<!DOCTYPE html>
<html>
<head>
	<title>Socialhack | Хакатон от команды R.I.I.C</title>
</head>
<body>
	<center>
		<h1>Добро пожаловать на страницу хакатона от участников команды R.I.I.C!</h1>
		<hr>
		<p>До сдачи проектов осталось:</p>
		<h2 id="timer">00:00:00</h2>
		<a href="/play.php">Перейти</a>
	</center>
	<script>
	// Set the date we're counting down to
	var countDownDate = new Date("Jun 9, 2019 12:00:00").getTime();

	// Update the count down every 1 second
	var x = setInterval(function() {

	  // Get today's date and time
	  var now = new Date().getTime();

	  // Find the distance between now and the count down date
	  var distance = countDownDate - now;

	  // Time calculations for days, hours, minutes and seconds
	  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
	  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
	  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
	  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

	  // Display the result in the element with id="demo"
	  document.getElementById("timer").innerHTML = hours + ":"
	  + minutes + ":" + seconds;

	  // If the count down is finished, write some text 
	  if (distance < 0) {
	    clearInterval(x);
	    document.getElementById("timer").innerHTML = "Время вышло";
	  }
	}, 1000);
	</script>
</body>
</html>